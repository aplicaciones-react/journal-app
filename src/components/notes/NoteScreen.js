import React from 'react'
import { NotesAppBar } from './NotesAppBar'

export const NoteScreen = () => {
    return (
        <div className="notes__main-content">
            
            <NotesAppBar />

            <div className="notes__content">

                <input 
                    type="text"
                    placeholder="Some awesome title"
                    className="notes__title-input"
                    autoComplete="off"
                />

                <textarea className="notes__textarea" placeholder="What happend today?"></textarea>

                <div className="notes__image">
                    <img 
                        src="https://upload.wikimedia.org/wikipedia/commons/7/75/AmanecerDesdeElGarb%C3%AD.JPG" alt="imagse"
                    />
                </div>

            </div>

        </div>
    )
}
