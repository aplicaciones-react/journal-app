import React from 'react'

export const JournalEntry = () => {
    return (
        <div className="journal__entry pointer">
            
            <div 
                className="journal__entry-picture"
                style={
                    {
                        backgroundSize:'cover',
                        backgroundImage: 'url(https://upload.wikimedia.org/wikipedia/commons/7/75/AmanecerDesdeElGarb%C3%AD.JPG)',
                    }
                }
            ></div>

            <div className="journal__entry-body">
                <p className="journal__entry-title">
                    UN nuevo dia
                </p>
                <p className="journal__entry-content">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda corporis voluptate blanditiis voluptatibu.
                </p>
            </div>

            <div className="journal__entry-date-box">
                <span>Monday</span>
                <h4>28</h4>
            </div>

        </div>
    )
}
